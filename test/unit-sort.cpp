#include "doctest/doctest.h"

#include "sort.hpp"

TEST_CASE("Sorts") {
    SUBCASE("Insert sort") {
        SUBCASE("Example 1") {
            std::vector<int> arr = {12, 3, 5, 20, 1, 200, 43, 3000, 13, 12, 9};
            std::vector<int> copy = arr;

            std::sort(arr.begin(), arr.end());
            insert_sort(copy);

            REQUIRE(arr == copy);
        }

        SUBCASE("Example 2") {
            std::vector<int> arr = {3, 3, 1, 2, 5};
            std::vector<int> copy = arr;

            std::sort(arr.begin(), arr.end());
            insert_sort(copy);

            REQUIRE(arr == copy);
        }
    }

    SUBCASE("Insert sort decreasing") {
        SUBCASE("Example 1") {
            std::vector<int> arr = {12, 3, 5, 20, 1, 200, 43, 3000, 13, 12, 9};
            std::vector<int> copy = arr;

            std::sort(arr.begin(), arr.end());
            std::reverse(arr.begin(), arr.end());
            insert_sort_decreasing(copy);

            REQUIRE(arr == copy);
        }

        SUBCASE("Example 2") {
            std::vector<int> arr = {3, 3, 1, 2, 5};
            std::vector<int> copy = arr;

            std::sort(arr.begin(), arr.end());
            std::reverse(arr.begin(), arr.end());
            insert_sort_decreasing(copy);

            REQUIRE(arr == copy);
        }
    }

    SUBCASE("Basic merge sort") {
        SUBCASE("Example 1") {
            std::vector<int> arr = {12, 3, 5, 20, 1, 200, 43, 3000, 13, 12, 9};
            std::vector<int> copy = arr;

            std::sort(arr.begin(), arr.end());
            basic_merge_sort(copy);

            REQUIRE(arr == copy);
        }

        SUBCASE("Example 2") {
            std::vector<int> arr = {3, 3, 1, 2, 5};
            std::vector<int> copy = arr;

            std::sort(arr.begin(), arr.end());
            basic_merge_sort(copy);

            REQUIRE(arr == copy);
        }
    }
}
