cmake_minimum_required(VERSION 3.5)

project(Algorithms VERSION 0.1 LANGUAGES CXX)

set(MAIN_PROJECT OFF)
if (CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
      set(MAIN_PROJECT ON)
endif()

set(CMAKE_CXX_STANDARD            20)
set(CMAKE_CXX_STANDARD_REQUIRED   ON)
set(CMAKE_CXX_FLAGS_DEBUG         "${CMAKE_CXX_FLAGS_DEBUG} -fno-omit-frame-pointer -fsanitize=address")
set(CMAKE_LINKER_FLAGS_DEBUG      "${CMAKE_LINKER_FLAGS_DEBUG} -fno-omit-frame-pointer -fsanitize=address")

option(BUILD_TESTS        "Build unit tests"          ${MAIN_PROJECT})

set(HEADER_LIST
        include/sort.hpp
    )

add_library(Algorithms INTERFACE)
target_include_directories(Algorithms INTERFACE "include")

if (${BUILD_TESTS})
      message("-- Tests enabled")
      enable_testing()
      add_subdirectory(test)
endif()
