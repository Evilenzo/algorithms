#pragma once

#include <vector>
#include <queue>
#include <concepts>


template <typename T>
concept Compareable = requires(T a, T b) {
    { a <=> b } -> std::same_as<bool>;
}
|| requires(T a, T b) {
    { a < b } -> std::same_as<bool>;
    { a <= b } -> std::same_as<bool>;
    { a == b } -> std::same_as<bool>;
};

template <typename T>
concept Container = requires(T t) {
    std::input_or_output_iterator<typename T::iterator>;

    { t.begin() } -> std::input_or_output_iterator;
    { t.end() } -> std::input_or_output_iterator;
    { t.size() } -> std::same_as<std::size_t>;
    { t.empty() } -> std::same_as<bool>;

    t[0];

    Compareable<typename T::value_type>;
};

/////////////////////////////////////////////////////////

/**
 * @brief Insert Sort
 * @details O(n^2)
 */
template <Container T>
inline constexpr void insert_sort(T& arr) {
    typedef typename T::value_type Value;

    if (arr.size() >= 2) {
        for (int64_t j = 1; j < arr.size(); ++j) {
            Value key = arr[j];

            int64_t i = j - 1;
            while (i >= 0 && key < arr[i]) {
                arr[i + 1] = arr[i];
                --i;
            }

            arr[i + 1] = key;
        }
    }
}

/**
 * @brief Decreasing Insert Sort
 * @details O(n^2)
 */
template <Container T>
inline constexpr void insert_sort_decreasing(T& arr) {
    typedef typename T::value_type Value;

    for (int64_t j = 1; j < arr.size(); ++j) {
        Value key = arr[j];

        int64_t i = j - 1;
        while (i >= 0 && arr[i] < key) {
            arr[i + 1] = arr[i];
            i -= 1;
        }

        arr[i + 1] = key;
    }
}

/**
 * @brief Basic merge operation
 * @details O(n) but big constant
 */
template <Container T>
inline constexpr void basic_merge(T& arr, std::size_t p, std::size_t q, std::size_t r) {
    typedef typename T::value_type Value;

    std::queue<Value> left;
    std::queue<Value> right;

    for (int i = 0; i <= q - p; ++i) {
        left.push(arr[p + i]);
    }

    for (int i = 0; i <= r - q - 1; ++i) {
        right.push(arr[q + 1 + i]);
    }

    for (std::size_t i = p; i <= r; ++i) {
        if (right.empty() || (!left.empty() && left.front() <= right.front())) {
            arr[i] = left.front();
            left.pop();
        } else {
            arr[i] = right.front();
            right.pop();
        }
    }
}

/**
 * @brief Basic merge sort (for recursion)
 * @details O(n) but big constant
 */
template <Container T>
inline constexpr void basic_merge_sort(T& arr, std::size_t p, std::size_t r) {
    if (p < r) {
        std::size_t q = (p + r) / 2;

        basic_merge_sort(arr, p, q);
        basic_merge_sort(arr, q + 1, r);
        basic_merge(arr, p, q, r);
    }
}

/**
 * @brief Basic merge sort (for user)
 * @details O(n) but big constant
 */
template <Container T>
inline constexpr void basic_merge_sort(T& arr) {
    basic_merge_sort(arr, 0, arr.size() - 1);
}
